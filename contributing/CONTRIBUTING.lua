-- SPDX-FileCopyrightText: Huawei Inc.
--
-- SPDX-License-Identifier: Apache-2.0

--- Filter out toctree and contents directives
-- This function is a element-transforming function to be used as a lua-filter
-- for pandoc. You can find it passed to pandoc as part of CONTRIBUTING.sh
-- tool. This is needed when converting a set of reST documents to a markdown
-- one where directives like `toctree` and `contents` are not supported and end
-- up translated literally.
function Div(el)
  local class = el["c"][1][2][1]
  if class == "toctree" or class == "contents" then
    return {}
  else
    return nil
  end
end
