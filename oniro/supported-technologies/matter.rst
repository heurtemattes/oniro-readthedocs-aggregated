.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

Matter
######

The Matter protocol (formerly Project Connected Home over IP, CHIP) is an
application layer protocol for IoT devices and controllers. It is based on IPv6
and UDP/TCP and describes the message format, security, interaction model and
data model among other things. Matter is a new standard with its first release
in September 2022. For more details on Matter, `see <https://csa-iot.org/all-solutions/matter/>`__.

Matter Support in Oniro
***********************

The Matter working group has developed an `open-source SDK
<https://github.com/project-chip/connectedhomeip>`__  for Matter in tandem
with the specification. This reference implementation has been used for all
testing events towards the release of the final specification.

To support Matter in Oniro, we integrated the Matter open-source SDK in our build
with a Yocto recipe that builds the first release of the SDK. In the future we will
follow the release branch that backports fixes, until there will be a new release.

So far this work has been focused on the Linux flavour. The Matter core
components, libraries and a few examples are working. No work has been done on
the Zephyr flavour yet, but it is on the Oniro roadmap to support Matter on
Zephyr as well.

Matter is not part of the default images provided by Oniro. To add it to your
custom image, you would need to add it to the image recipe, or append
it in your `local.conf` to every image you build e.g.:

.. code-block:: console

        IMAGE_INSTALL:append = " matter"
