# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: CC-BY-4.0

six
recommonmark
sphinx-tabs
sphinxcontrib_plantuml
